import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Chat from '../chat/index';
import { compose } from 'recompose';
import roomsModel from '../../models/rooms';
import _ from 'lodash';

const styles = theme => ({
  leftColumn: {
    padding: theme.spacing.unit
  },
  rightColumn: {
    padding: theme.spacing.unit
  }
});

class Home extends Component {
  componentWillMount() {
    roomsModel.fetch(
      (response) => {
        this.props.dispatch({
          type: 'UPDATE_ROOMS', payload: response.data
        });
      },
      () => {
        this.props.dispatch({
          type: 'UPDATE_ROOMS', payload: null
        });
      }
    );
  }

  componentDidUpdate() {
    const { rooms, selectedRoom, dispatch } = this.props;
    let firstRoomId;
    if (_.size(rooms) && !selectedRoom) {
      firstRoomId = _.get(_.first(rooms), 'id');
      dispatch({
        type: 'SELECT_ROOM', payload: firstRoomId
      });
    }
  }

  handleRoomSelect = (roomId) => {
    this.props.dispatch({
      type: 'SELECT_ROOM', payload: roomId
    });
  };

  renderRooms() {
    const { rooms } = this.props;
    return (
      <Grid container spacing={0}>
        {
          _.map(rooms, (room) => {
            return (
              <Grid item xs={12}>
                <Button variant='outlined' fullWidth size='large' onClick={this.handleRoomSelect.bind(null, room.id)}>
                  { room.name }
                </Button>
              </Grid>
            );
          })
        }
      </Grid>
    );
  }

  render() {
    const {
      classes
    } = this.props;
    return (
      <div>
        <AppBar position='static'>
          <Toolbar>
            <Typography variant='title' color='inherit'>
              Gitter Basic Client
            </Typography>
          </Toolbar>
        </AppBar>
        <Grid container>
          <Grid item xs={3} className={classes.leftColumn}>
            { this.renderRooms() }
          </Grid>
          <Grid item xs={9} className={classes.rightColumn}>
            <Chat />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default compose(
  connect((state) => ({
    rooms: _.get(state, 'rooms', []),
    selectedRoom: _.get(state, 'selectedRoom', null)
  })),
  withStyles(styles)
)(Home);