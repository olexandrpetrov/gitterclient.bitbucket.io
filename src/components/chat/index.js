import React, { Component } from 'react';
import roomsModel from '../../models/rooms';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import _ from "lodash";

class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textMessage: ''
    };
  }

  componentDidUpdate() {
    const {
      selectedRoom,
      chatMessages
    } = this.props;
    if (selectedRoom && !_.size(chatMessages)) { // initial messages fetch
      this.fetchMessages();
    }
  }

  fetchMessages = () => {
    const {
      selectedRoom,
      dispatch,
    } = this.props;
    roomsModel.chat.fetchMessages(
      selectedRoom,
      (response) => {
        dispatch({
          type: 'UPDATE_CHAT_MESSAGES',
          payload: response.data
        });
      }
    );
  };

  handleTextMessageChange = (event) => {
    const value = _.get(event, 'target.value', '');
    this.setState({
      textMessage: value
    });
  };

  handleMessageSend = (event) => {
    const { selectedRoom } = this.props;
    const { textMessage } = this.state;
    event.preventDefault();
    event.stopPropagation();
    roomsModel.chat.addMessage(
      selectedRoom,
      textMessage,
      () => {
        this.setState({
          textMessage: ''
        });
        this.fetchMessages();
      });
  };

  render() {
    const { chatMessages } = this.props;
    return(
      <div>
        <div>
          <form noValidate autoComplete='off' onSubmit={this.handleMessageSend}>
            <Grid container>
              <Grid item xs={10}>
                <TextField
                  placeholder='type a message'
                  value={this.state.textMessage}
                  onChange={this.handleTextMessageChange}
                  margin='none'
                  autoFocus
                  fullWidth
                />
              </Grid>
              <Grid item xs={2}>
                <Button type='submit'>
                  send
                </Button>
              </Grid>
            </Grid>
          </form>
        </div>
        <div>
          {
            _.map(chatMessages, (message) => {
              return (
                <div key={message.id}>{ `@${message.fromUser.username}: ${message.text}` }</div>
              );
            })
          }
        </div>
      </div>
    );
  }
}

export default compose(
  connect((state) => ({
    selectedRoom: _.get(state, 'selectedRoom', null),
    chatMessages: _.get(state, 'chatMessages', [])
  }))
)(Chat);

