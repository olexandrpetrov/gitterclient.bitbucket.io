import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    height: '100%'
  }
});

class Login extends Component {
  render() {
    const {
      classes
    } = this.props;
    return(
      <Grid container justify='center' alignItems='center' className={classes.root}>
        <Grid item>
          <Button href={'/auth/gitter'}>
            Login
          </Button>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(Login);