const app = (state, action) => {
  switch (action.type) {
    case 'UPDATE_ROOMS':
      return {
        ...state,
        rooms: action.payload
      };
    case 'SELECT_ROOM':
      return {
        ...state,
        selectedRoom: action.payload
      };
    case 'UPDATE_CHAT_MESSAGES':
      return {
        ...state,
        chatMessages: action.payload
      };
    default:
      return state;
  }
};

export default app;