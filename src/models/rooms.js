import axios from 'axios';
import _ from 'lodash';

const rooms = {
  fetch: (resolve = _.noop, reject = _.noop) => {
    axios.get('/api/rooms')
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        if (_.get(error, 'response.status') === 401) {
          window.location = '/';
        }
        reject(error);
      });
  },
  chat: {
    fetchMessages: (roomId, resolve = _.noop, reject = _.noop) => {
      axios.get(`/api/messages/${roomId}`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          if (_.get(error, 'response.status') === 401) {
            window.location = '/';
          }
          reject(error);
        });
    },
    addMessage: (roomId, text, resolve = _.noop, reject = _.noop) => {
      axios.post(`/api/messages/${roomId}`, {
        text
      })
        .then(function () {
          resolve();
        })
        .catch(function (error) {
          if (_.get(error, 'response.status') === 401) {
            window.location = '/';
          }
          reject(error);
        });
    }
  }
};

export default rooms;