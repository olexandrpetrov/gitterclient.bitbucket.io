import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import App from './App';
// import registerServiceWorker from './registerServiceWorker';
import { createStore } from 'redux';

import AppReducer from './App.reducer';
const initialState = {
  rooms: null
};
const store = createStore(AppReducer, initialState);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
// registerServiceWorker();
