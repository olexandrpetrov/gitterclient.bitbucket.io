import React, { Component } from 'react';
import Login from './components/login';
import Home from './components/Home';
import './App.css';

const searchToComponent = {
  '': <Login />,
  '?home': <Home />
};

class App extends Component {
  render() {
    return (
      <div className='App'>
        { searchToComponent[window.location.search] }
      </div>
    );
  }
}

export default App;
