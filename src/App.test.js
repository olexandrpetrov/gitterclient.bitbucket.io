import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import AppReducer from './App.reducer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

test('reducer should update rooms', () => {
  const initialState = {};
  const expectedResult = {
    'rooms': [{
      'id': 1
    }, {
      'id': 2
    }]
  };
  const result = AppReducer(
    initialState,
    {
      type: 'UPDATE_ROOMS',
      payload: [{
        id: 1
      }, {
        id: 2
      }]
    }
  );
  expect(result).toEqual(expectedResult);
});

test('reducer should update selected room', () => {
  const initialState = {};
  const expectedResult = {
    'selectedRoom': 789
  };
  const result = AppReducer(
    initialState,
    {
      type: 'SELECT_ROOM',
      payload: 789
    }
  );
  expect(result).toEqual(expectedResult);
});

test('reducer should update chat messages', () => {
  const initialState = {};
  const expectedResult = {
    'chatMessages': [{
      'text': '1'
    }, {
      'text': '2'
    }]
  };
  const result = AppReducer(
    initialState,
    {
      type: 'UPDATE_CHAT_MESSAGES',
      payload: [{
        text: '1'
      }, {
        text: '2'
      }]
    }
  );
  expect(result).toEqual(expectedResult);
});
