/*jshint globalstrict:true, trailing:false, unused:true, node:true */
"use strict";

var express = require('express');
var passport = require('passport');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var router = express.Router();
var GitterStrategy = require('passport-gitter');
var _ = require('lodash');
var request = require('request');

var gitterHost = process.env.HOST || 'https://gitter.im';
var port = process.env.PORT || 7000;
var staticPath = express.static(__dirname + '/build');

var clientId = process.env.GITTER_KEY ? process.env.GITTER_KEY.trim() : '09c1d0f40a67066849371760166a144f92de5643';
var clientSecret = process.env.GITTER_SECRET ? process.env.GITTER_SECRET.trim() : '8571b57aece929ad673eee0856d6cbfad3cdcdbb';
var callbackURL = 'http://gitterclient.fr.openode.io/auth/gitter/callback';
var gitterStrategyConfig = new GitterStrategy({
    clientID: clientId,
    clientSecret: clientSecret,
    callbackURL: callbackURL,
    autoResolveCallback: true
  },
  function (accessToken, refreshToken, profile, done) {
    done(null, { accessToken, profile});
  }
);

var app = express();
app.use(staticPath);
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({secret: 'keyboard cat', resave: true, saveUninitialized: true}));
app.use(passport.initialize());
app.use(passport.session());
app.use(router);

passport.use(gitterStrategyConfig);
passport.serializeUser(function (user, done) {
  done(null, JSON.stringify(user));
});
passport.deserializeUser(function (user, done) {
  done(null, JSON.parse(user));
});

var gitter = {
  fetch: function(path, token, cb) {
    var options = {
      url: gitterHost + path,
      headers: {
        'Authorization': 'Bearer ' + token
      }
    };
    request(options, function (err, res, body) {
      if (err) return cb(err);
      if (res.statusCode === 200) {
        cb(null, JSON.parse(body));
      } else {
        cb('err' + res.statusCode);
      }
    });
  },
  fetchRooms: function(user, token, cb) {
    this.fetch('/api/v1/user/' + user.id + '/rooms', token, function(err, rooms) {
      cb(err, rooms);
    });
  },
  fetchChatMessages: function (roomId, token, cb) {
    this.fetch('/api/v1/rooms/' + roomId + '/chatMessages?limit=50', token, function(err, messages) {
      cb(err, messages);
    });
  },
  postChatMessage: function(roomId, text, token, cb) {
    request({
      method: 'POST',
      url: gitterHost + '/api/v1/rooms/' + roomId + '/chatMessages',
      headers: {
        'Authorization': 'Bearer ' + token
      },
      form: {
        text: text
      }
    }, function(err, response) {
      cb(err, response);
    });
  }
};

app.get('/auth/gitter',
  passport.authenticate('gitter', { scope: ['user:email'] }));

app.get('/auth/gitter/callback',
  passport.authenticate('gitter', { failureRedirect: '/?error' }),
  function (req, res) {
    res.redirect('/?home');
  });

app.get('/api/rooms', function(req, res) {
  var user, profile, token;
  var userRaw = _.get(req, 'session.passport.user');
  if (!userRaw || _.isEmpty(userRaw)) { res.status(401).send(); return; }
  user = JSON.parse(userRaw);
  profile = _.get(user, 'profile');
  token = _.get(user, 'accessToken');
  gitter.fetchRooms(profile, token, function(err, rooms) {
    if (err) { return res.sendStatus(500, err); }
    res.json(rooms);
  });
});

app.get('/api/messages/:roomId', function(req, res) {
  var user, token;
  var userRaw = _.get(req, 'session.passport.user');
  if (!userRaw || _.isEmpty(userRaw)) { res.status(401).send(); return; }
  user = JSON.parse(userRaw);
  token = _.get(user, 'accessToken');
  gitter.fetchChatMessages(req.params.roomId, token, function(err, messages) {
    if (err) { return res.sendStatus(500, err); }
    res.json(messages);
  });
});

app.post('/api/messages/:roomId', function(req, res) {
  var user, token;
  var userRaw = _.get(req, 'session.passport.user');
  if (!userRaw || _.isEmpty(userRaw)) { res.status(401).send(); return; }
  user = JSON.parse(userRaw);
  token = _.get(user, 'accessToken');
  gitter.postChatMessage(req.params.roomId, req.body.text, token, function(err, response) {
    if (err) { return res.sendStatus(500, err); }
    return res.sendStatus(200);
  });
});

app.listen(port);
console.log('Demo app running at http://localhost:' + port);